﻿namespace PSMUI
{
    partial class PSMUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PSMUI));
            this.GameList = new System.Windows.Forms.Panel();
            this.ScreenRes = new System.Windows.Forms.Label();
            this.screenResolution = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // GameList
            // 
            this.GameList.AutoScroll = true;
            this.GameList.BackColor = System.Drawing.Color.Teal;
            this.GameList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.GameList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.GameList.Location = new System.Drawing.Point(0, 33);
            this.GameList.Name = "GameList";
            this.GameList.Size = new System.Drawing.Size(1264, 598);
            this.GameList.TabIndex = 3;
            // 
            // ScreenRes
            // 
            this.ScreenRes.AutoSize = true;
            this.ScreenRes.ForeColor = System.Drawing.Color.White;
            this.ScreenRes.Location = new System.Drawing.Point(12, 9);
            this.ScreenRes.Name = "ScreenRes";
            this.ScreenRes.Size = new System.Drawing.Size(60, 13);
            this.ScreenRes.TabIndex = 4;
            this.ScreenRes.Text = "Resoultion:";
            // 
            // screenResolution
            // 
            this.screenResolution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.screenResolution.FormattingEnabled = true;
            this.screenResolution.Items.AddRange(new object[] {
            "1280x670",
            "1920x1080",
            "800x480",
            "960x544",
            "1280x800",
            "1920x1200"});
            this.screenResolution.Location = new System.Drawing.Point(78, 6);
            this.screenResolution.Name = "screenResolution";
            this.screenResolution.Size = new System.Drawing.Size(141, 21);
            this.screenResolution.TabIndex = 1;
            // 
            // PSMUI
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1264, 631);
            this.Controls.Add(this.screenResolution);
            this.Controls.Add(this.ScreenRes);
            this.Controls.Add(this.GameList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PSMUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Game Browser";
            this.Load += new System.EventHandler(this.PSMUI_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.PSMUI_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.PSMUI_DragEnter);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel GameList;
        private System.Windows.Forms.Label ScreenRes;
        private System.Windows.Forms.ComboBox screenResolution;
    }
}

